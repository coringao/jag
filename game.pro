######################################################################
# This file is part of JAG, Puzzle game where the goal is to break all
# the target pieces in each level and do this before the time runs out.
#
# Many thanks to XlabSoft & Ind. Infosystems, the originals authors of JAG.
#
# Copyright holder 2009-2012 Developed by XlabSoft & Industrial Infosystems
# Work continued by 2017-2020 Carlos Donizete Froes [a.k.a coringao]
#
# JAG is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
######################################################################

TEMPLATE = app
TARGET = jag
INCLUDEPATH += .

QT += gui core widgets opengl xml x11extras
CONFIG += link_pkgconfig
PKGCONFIG += sdl2 SDL2_mixer
QMAKE_CXXFLAGS += -g -std=gnu++17 -D_FORTIFY_SOURCE=2
QMAKE_LFLAGS += -fPIE -pie -Wl,--as-needed -Wl,-z,now
LIBS += -lGL -lX11 -lSDL2 -lSDL2_mixer -lpthread -lXrandr

OBJECTS_DIR += src
MOC_DIR += src
RCC_DIR += src
UI_DIR += src

unix: {
	target.path = /usr/games/
	INSTALLS += target
	datas.path = /usr/share/games/jag
	datas.files = data
	INSTALLS += datas
}

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += src/baseitem.h \
           src/bighammertool.h \
           src/bombtool.h \
           src/clocktool.h \
           src/consttools.h \
           src/defines.h \
           src/displaywrapper.h \
           src/gamebackground.h \
           src/gamebonus.h \
           src/gameitem.h \
           src/gamemenu.h \
           src/gameprofile.h \
           src/gamescene.h \
           src/gamesound.h \
           src/gamestat.h \
           src/gamestock.h \
           src/gametools.h \
           src/gamewidget.h \
           src/hammertool.h \
           src/mixertool.h \
           src/randomkilltool.h \
           src/scaler.h \
           src/scene_if.h \
           src/smallhammertool.h \
           src/thundertool.h \
           src/twintool.h \
           src/unblocktool.h \
           src/version.h
FORMS += src/menu.ui
SOURCES += src/baseitem.cpp \
           src/bighammertool.cpp \
           src/bombtool.cpp \
           src/clocktool.cpp \
           src/consttools.cpp \
           src/displaywrapper.cpp \
           src/gamebackground.cpp \
           src/gamebonus.cpp \
           src/gamecontrol.cpp \
           src/gameitem.cpp \
           src/gamemenu.cpp \
           src/gamepaint.cpp \
           src/gameprofile.cpp \
           src/gamescene.cpp \
           src/gamesound.cpp \
           src/gamestat.cpp \
           src/gamestatics.cpp \
           src/gamestock.cpp \
           src/gametools.cpp \
           src/gamewidget.cpp \
           src/gamexml.cpp \
           src/hammertool.cpp \
           src/main.cpp \
           src/menucontrol.cpp \
           src/mixertool.cpp \
           src/randomkilltool.cpp \
           src/scaler.cpp \
           src/smallhammertool.cpp \
           src/thundertool.cpp \
           src/twintool.cpp \
           src/unblocktool.cpp
RESOURCES += src/resources.qrc
TRANSLATIONS += data/lang/jag_de.ts \
                data/lang/jag_es.ts \
                data/lang/jag_fr.ts \
                data/lang/jag_it.ts \
                data/lang/jag_nl.ts \
                data/lang/jag_pt.ts \
                data/lang/jag_ru.ts
